FROM alpine
RUN     apk --no-cache add libcap nginx  && \
    setcap 'cap_net_bind_service=+ep' /usr/sbin/nginx && \
    mkdir -p /run/nginx && chown nginx:nginx /run/nginx && \
    ln -sf /dev/stdout /var/log/nginx/access.log  && \
    ln -sf /dev/stderr /var/log/nginx/error.log
COPY nginx-custom.conf /etc/nginx/http.d/default.conf
WORKDIR /usr/share/nginx/html
COPY build/* /usr/share/nginx/html/
USER nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

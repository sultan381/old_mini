import styled from "@emotion/styled";
import FooterCoinImage from "./images/footer-coins.webp";
import FooterLogoImage from "./images/logo-footer@0.5x.png";
import { SocialButtons } from "../UI";
import { Typography } from "@material-ui/core";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";

const FooterWrapper = styled.div`
  width: 100%;
  height: 500px;
  position: relative;
  margin-top: 50px;
`;

const Background = styled.div`
  position: absolute;
  width: 100%;
  height: 406px;
  z-index: 1;
  overflow: hidden;
  bottom: 0;
`;

const BackgroundElement = styled.div`
  ${({ theme }) => `
  position: absolute;
  width: 100%;
  height: 406px;
  background-color: ${theme.palette.type === "dark" ? "#000000" : "#303030"};
  z-index: 2;
  transform: skewY(3deg);
  bottom: -71px;
  `}
`;

const FooterCoins = styled.div`
  width: 576px;
  height: 475px;
  position: absolute;
  right: 70px;
  top: -162px;
  z-index: 3;
  > img {
    width: 100%;
  }
  @media (max-width: 1600px) {
    width: ${576 / 1.2}px;
    height: ${475 / 1.2}px;
    right: 0;
    top: 0;
  }
  @media (max-width: 1100px) {
    display: none;
  }
`;

const FooterElement = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding-top: 211px;
  z-index: 3;
  position: relative;
`;

const FooterLogo = styled.div`
  width: 80px;
  height: 80px;
  border: 1px solid #000000;
  background-color: #ffffff;
  border-radius: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  > img {
    width: 80%;
  }
`;

function Footer() {
  return (
    <FooterWrapper>
      <Background>
        <BackgroundElement />
      </Background>
      <FooterCoins>
        <img src={FooterCoinImage} alt="" />
      </FooterCoins>
      <FooterElement>
        <FooterLogo>
          <img src={FooterLogoImage} alt="" />
        </FooterLogo>
        <Typography
          variant="h4"
          component="h4"
          css={(theme) => css`
            font-weight: 700;
            color: #fff;
            margin-top: 1rem;
          `}
          gutterBottom
        >
          My Mini Miners
        </Typography>
        <SocialButtons color={"#fff"} />
      </FooterElement>
    </FooterWrapper>
  );
}

export default Footer;

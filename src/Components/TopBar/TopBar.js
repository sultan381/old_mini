import PropTypes from "prop-types";
import { Box, Container, Grid } from "@material-ui/core";
import Brightness7Icon from "@material-ui/icons/Brightness7";
import Brightness3Icon from "@material-ui/icons/Brightness3";
import styled from "@emotion/styled";
import SocialButtons from "../UI/SocialButtons/SocialButtons";
import Switch from "@material-ui/core/Switch";
import { withStyles } from "@material-ui/styles";

const TopBarHolder = styled.div`
  position: fixed;
  width: 100%;
  z-index: 200;
`;
const SocialButtonHolder = styled.div`
  background-color: #e2e2e22e;
  border-radius: 25px;
  box-shadow: 1px 1px 4px #66339938;
  padding: 0.1rem 0.5rem;
  a {
    filter: drop-shadow(0px 0px 2px #66339938);
  }
`;
const LogoHolder = styled.div`
  width: 70px;
  height: 70px;
  background-color: #ffffff;
  border-radius: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  img {
    width: 80%;
  }
`;

const IOSSwitch = withStyles((theme) => ({
  root: {
    width: 90,
    height: 50,
    padding: 0,
    borderRadius: 25,
    boxShadow: "1px 2px 5px rgba(0, 0, 0, 0.12)",
  },

  switchBase: {
    padding: 5,
    "&$checked": {
      transform: "translateX(40px)",
      color: theme.palette.common.white,
      "& + $track": {
        backgroundColor: "#ffffff",
        opacity: 0.1,
      },
    },
    "&$focusVisible $thumb": {
      color: "#ffffff",
    },
  },
  thumb: {
    width: 40,
    height: 40,
  },
  track: {
    borderRadius: 25 / 2,
    backgroundColor: "#ffffff",
    opacity: 0.6,
    transition: theme.transitions.create(["background-color", "border"]),
  },
  checked: {},
  focusVisible: {},
}))(({ classes, ...props }) => {
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
      {...props}
    />
  );
});

function TopBar({ theme, setTheme }) {
  return (
    <TopBarHolder>
      <Container maxWidth={false} disableGutters>
        <Grid container spacing={3}>
          <Grid container item xs={2} alignItems="center">
            <Box p={[1, 1, 3]}>
              <LogoHolder>
                <img src={require("./images/logo.png").default} alt="logo" />
              </LogoHolder>
            </Box>
          </Grid>
          <Grid container item xs={8} justify="center" alignItems="center">
            <Box p={[1, 1, 2]}>
              <SocialButtonHolder>
                <SocialButtons color="#fff" />
              </SocialButtonHolder>
            </Box>
          </Grid>
          <Grid container item xs={2} justify="flex-end" alignItems="center">
            <Box p={[1, 1, 2]}>
              <IOSSwitch
                checked={!theme}
                onChange={() => setTheme(!theme)}
                name="checkedA"
                icon={
                  <Brightness7Icon
                    style={{
                      fontSize: 40,
                      color: "#facd01",
                      backgroundColor: "#fff",
                      borderRadius: 40,
                    }}
                  />
                }
                checkedIcon={
                  <Brightness3Icon
                    style={{
                      fontSize: 40,
                      color: "#facd01",
                      backgroundColor: "#fff",
                      borderRadius: 40,
                    }}
                  />
                }
              />
            </Box>
          </Grid>
        </Grid>
      </Container>
    </TopBarHolder>
  );
}

export default TopBar;

TopBar.propTypes = {
  setTheme: PropTypes.func.isRequired,
  theme: PropTypes.bool.isRequired,
};

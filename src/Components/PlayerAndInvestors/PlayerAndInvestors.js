import styled from "@emotion/styled";
import {
  Box,
  Container,
  Grid,
  List,
  ListItem,
  ListItemText,
  Typography,
} from "@material-ui/core";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";

const Content = styled.div`
  ${({ theme }) => `
  width: 100%;
  // height: 104rem;
  background-color: ${theme.palette.type === "dark" ? "#ffffff" : "#df665f"};
  padding-top: 140px;
  @media (max-width: 600px) {
   padding-top: 40px;
  }
  `}
`;

function PlayerAndInvestors() {
  return (
    <Content>
      <Container maxWidth="lg">
        <Box p={[3, 0, 0]}>
          <Grid container spacing={4} justify="center">
            <Grid container item xs={12} justify="center">
              <Typography variant="h3" component="h3" gutterBottom>
                <Box
                  component="span"
                  css={(theme) => css`
                    color: ${theme.palette.type === "dark" ? "#000" : "#fff"};
                  `}
                >
                  Become the Ultimate{" "}
                </Box>
                <span
                  css={(theme) => css`
                    color: ${theme.palette.type === "dark"
                      ? "#7919ae"
                      : "#000"};
                  `}
                >
                  Miner!!
                </span>
              </Typography>
              <Typography variant="body1" component="p" gutterBottom>
                <Box color="#000" align="center" mb={5}>
                  <b>
                    Gnomes are funny-looking, adorable creatures who love to
                    sleep, eat, and find treasures.
                  </b>
                  , The best thing they do is find a treasure for their masters,
                  However, being a Gnome Master is not an easy task. in your
                  journey, you will have to compete with other gnome masters for
                  limited gnomes and treasure supply. to become the best, you
                  will have to :
                </Box>
              </Typography>
            </Grid>
            <Grid container item xs={12} md={6} alignItems="center">
              <img
                src={require("./images/investors.png").default}
                width="100%"
                alt="investors"
              />
            </Grid>
            <Grid container item xs={12} md={6} justify="flex-start">
              <Typography variant="h3" component="h3" gutterBottom>
                <Box
                  component="span"
                  css={(theme) => css`
                    color: ${theme.palette.type === "dark"
                      ? "#7919ae"
                      : "#fff"};
                  `}
                >
                  Gameplay
                </Box>
              </Typography>

              <Box color="#000">
                <List>
                  <ListItem>
                    <FiberManualRecordIcon
                      style={{
                        marginRight: "15px",
                        marginBottom: "auto",
                        marginTop: "5px",
                      }}
                    />
                    <ListItemText
                      primary="Collect Gnomes. Gnomes are the heart of the game. There are a total of 100 different gnomes, split into four rarities and 14 various collections.
Each gnome has a power property. The rarer the gnome is, the more power it will have."
                    />
                  </ListItem>
                  <ListItem>
                    <FiberManualRecordIcon
                      style={{
                        marginRight: "15px",
                        marginBottom: "auto",
                        marginTop: "5px",
                      }}
                    />
                    <ListItemText
                      primary="Merge Gnomes. Merging a gnome is a fun task but not an easy one.
To upgrade a gnome to its next level, you will have to get the same gnome twice and then merge them into a new upgraded one. The higher the gnome level, the more power it will have."
                    />
                  </ListItem>
                  <ListItem>
                    <FiberManualRecordIcon
                      style={{
                        marginRight: "15px",
                        marginBottom: "auto",
                        marginTop: "5px",
                      }}
                    />
                    <ListItemText
                      primary="Trade Gnomes.  while creating new gnomes can be fun and rewarding,
to fully unlock your potential, you will have to buy and sell gnomes with other players and get the specific gnomes you need to improve your collections."
                    />
                  </ListItem>
                  <ListItem>
                    <FiberManualRecordIcon
                      style={{
                        marginRight: "15px",
                        marginBottom: "auto",
                        marginTop: "5px",
                      }}
                    />
                    <ListItemText primary="Collections.  in the game, give gnomes a special bonus once they are completed. In your road to greatness, you should try to complete as many themes as possible to unlock great power multiples." />
                  </ListItem>
                  <ListItem>
                    <FiberManualRecordIcon
                      style={{
                        marginRight: "15px",
                        marginBottom: "auto",
                        marginTop: "5px",
                      }}
                    />
                    <ListItemText primary="Mining. Every time a new gnome is created, some of the coins he spent are going to the mining pool. Once a day, you will be able to claim Matic from the mining pool, based on your gnomes and collection's overall power." />
                  </ListItem>
                </List>
              </Box>
            </Grid>
            <Grid item xs={12} md={6}>
              <Typography
                gutterBottom
                variant="h3"
                component="h3"
                css={(theme) => css`
                  color: ${theme.palette.type === "dark" ? "#7919ae" : "#fff"};
                `}
              >
                Supply
              </Typography>
              <Typography variant="body1" component="p">
                <Box color="#000">
                  we believe that for things to be fun to collect, they need to
                  be rare, but how can we create a game for mass adaptation and
                  still make its items rare? Well,<b> we burn them.</b> <br />
                  Every time a player wants to upgrade a gnome, it will have to
                  burn two gnomes from the previous level. that 512 gnomes burn
                  only to max one gnome. Everyone can play the game and collect
                  gnomes with this approach, but only the hardcore collectors
                  will have the desired high-level gnomes. As gamers ourselves,
                  we believe that the players are the most important thing in
                  the game.
                </Box>
              </Typography>
            </Grid>
            <Grid container item xs={12} md={6} justify="center">
              <img
                src={require("./images/cards.png").default}
                width="100%"
                alt="investors"
              />
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Content>
  );
}

export default PlayerAndInvestors;

export { default as TopBar } from "./TopBar/TopBar";
export { default as Header } from "./Header/Header";
export { default as About } from "./About/About";
export { default as Footer } from "./Footer/Footer";
export { default as PlayerAndInvestors } from "./PlayerAndInvestors/PlayerAndInvestors";
export { default as FortuneWheel } from "./FortuneWheel/FortuneWheel";
export { default as Community } from "./Community/Community";

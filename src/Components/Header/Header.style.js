import styled from "@emotion/styled";

export const Head = styled.div`
  ${({ theme }) => `
  width: 100%;
  min-height: 1108px;
  padding-top: 190px;
  position: relative;
  overflow: hidden;
  background: rgb(51,153,153);
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  background-image: ${
    theme.palette.type === "dark"
      ? `url(${require("./images/header-dark-bg.png").default})`
      : "linear-gradient(180deg, rgba(51,153,153,1) 0%, rgba(232,252,211,1) 100%)"
  };
  @media (max-height: 950px) {
    padding-top: 100px;
    min-height: 950px;
  }
  @media (max-width: 600px) {
    padding-top: 100px;
    min-height: 700px;
    background-size: cover;
    background-image: ${
      theme.palette.type === "dark"
        ? `url(${require("./images/header-dark-bg-mobile.png").default})`
        : "linear-gradient(180deg, rgba(51,153,153,1) 0%, rgba(232,252,211,1) 100%)"
    };
  }
  @media (max-width: 400px) {
    min-height: 660px;
  }
  `}
`;

export const Background = styled.div`
  ${({ theme }) => `
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-size: contain;
  background-repeat: no-repeat;
  background-position: center bottom;
  background-image: url(${
    theme.palette.type === "dark"
      ? require("./images/header-dark-mountain.png").default
      : require("./images/header-light-bg.png").default
  });
  @media (max-width: 600px) {
    background-image: url(${
      theme.palette.type === "dark"
        ? require("./images/header-dark-mountain-mobile.png").default
        : require("./images/header-light-bg-mobile.png").default
    });
  }
  `}
`;

export const Shadow = styled.div`
  ${({ theme }) => `
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: bottom center;
  background-image: url(${
    theme.palette.type === "dark"
      ? require("./images/header-dark-shadow.png").default
      : "none"
  });
  @media (max-width: 600px) {
    background-image: url(${
      theme.palette.type === "dark"
        ? require("./images/header-dark-shadow-mobile.png").default
        : "none"
    });
  }
  `}
`;

export const Stone = styled.div`
  ${({ theme }) => `
  position: absolute;
  bottom: 150px;
  left: 50px;
  width: 100%;
  height: 387px;
  background-size: contain;
  background-repeat: no-repeat;
  background-position: bottom center;
  background-image: url(${
    theme.palette.type === "dark"
      ? require("./images/dark-stone-coin.png").default
      : "none"
  });
  @media (max-width: 1100px) {
    bottom: 60px;
  }
  @media (max-width: 600px) {
    bottom: 60px;
    left: -70px;
    width: 150%
  }
  `}
`;

export const FallingStars = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;

  .star-fall {
    position: relative;
    border-radius: 2px;
    width: 80px;
    height: 2px;
    overflow: hidden;
    transform: rotate(-45deg);
  }

  .star-fall:after {
    content: "";
    position: absolute;
    width: 50px;
    height: 2px;
    background: linear-gradient(
      to left,
      rgba(0, 0, 0, 0) 0%,
      rgba(255, 255, 255, 0.6) 100%
    );
    left: 100%;
    animation: star-fall 3.6s linear infinite;
  }

  .star-fall:nth-child(1) {
    left: 30%;
    bottom: -100px;
  }

  .star-fall:nth-child(1):after {
    animation-delay: 2.4s;
  }

  .star-fall:nth-child(2) {
    left: 60%;
    bottom: -200px;
  }

  .star-fall:nth-child(2):after {
    animation-delay: 2s;
  }

  .star-fall:nth-child(3) {
    left: 80%;
    bottom: -50px;
  }

  .star-fall:nth-child(3):after {
    animation-delay: 3.6s;
  }

  .star-fall:nth-child(4) {
    left: 10%;
    bottom: 100px;
  }

  .star-fall:nth-child(4):after {
    animation-delay: 0.2s;
  }

  @keyframes star-fall {
    20% {
      left: -100%;
    }
    100% {
      left: -100%;
    }
  }
`;

export const Stars = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  @keyframes animStar {
    from {
      transform: translateY(0px);
    }
    to {
      transform: translateY(-2000px);
    }
  }

  #stars {
    width: 1px;
    height: 1px;
    background: transparent;
    box-shadow: 1165px 1064px #fff, 326px 1081px #fff, 1613px 1333px #fff,
      2150px 623px #fff, 1149px 915px #fff, 1796px 696px #fff, 928px 1206px #fff,
      2435px 1880px #fff, 2794px 1606px #fff, 876px 827px #fff,
      2654px 704px #fff, 295px 655px #fff, 93px 1612px #fff, 2316px 399px #fff,
      1898px 1462px #fff, 530px 579px #fff, 2006px 1003px #fff,
      2185px 1668px #fff, 2141px 693px #fff, 2926px 529px #fff,
      1529px 546px #fff, 1583px 432px #fff, 1365px 594px #fff, 2551px 564px #fff,
      404px 400px #fff, 1322px 115px #fff, 633px 1943px #fff, 1149px 765px #fff,
      1117px 566px #fff, 1353px 544px #fff, 2676px 320px #fff, 1259px 857px #fff,
      446px 1731px #fff, 60px 142px #fff, 15px 758px #fff, 1031px 1329px #fff,
      2513px 1821px #fff, 338px 1104px #fff, 2179px 1677px #fff,
      1793px 407px #fff, 1782px 989px #fff, 2607px 1009px #fff,
      1446px 1113px #fff, 1151px 1498px #fff, 539px 1235px #fff,
      2157px 317px #fff, 2579px 1571px #fff, 1036px 620px #fff,
      1031px 435px #fff, 504px 412px #fff, 2036px 1456px #fff,
      2157px 1597px #fff, 815px 60px #fff, 2895px 42px #fff, 1222px 530px #fff,
      2978px 263px #fff, 2673px 1905px #fff, 2029px 1168px #fff,
      2143px 565px #fff, 2460px 165px #fff, 527px 1381px #fff,
      1624px 1573px #fff, 2894px 1304px #fff, 864px 1996px #fff,
      1705px 485px #fff, 2079px 659px #fff, 714px 1728px #fff,
      2436px 1104px #fff, 2821px 878px #fff, 1794px 367px #fff,
      1528px 1114px #fff, 779px 1106px #fff, 448px 1058px #fff, 611px 401px #fff,
      1936px 1449px #fff, 2691px 1019px #fff, 1029px 1223px #fff,
      2693px 1909px #fff, 1393px 1292px #fff, 71px 816px #fff,
      1259px 1219px #fff, 28px 1422px #fff, 844px 744px #fff, 1015px 941px #fff,
      2048px 383px #fff, 2376px 369px #fff, 2771px 660px #fff,
      2688px 1747px #fff, 1240px 1339px #fff, 2408px 139px #fff,
      2545px 859px #fff, 2773px 1758px #fff, 2580px 163px #fff,
      2211px 789px #fff, 2285px 42px #fff, 53px 1699px #fff, 729px 181px #fff,
      524px 1451px #fff, 1809px 520px #fff, 2529px 1791px #fff,
      2791px 1809px #fff, 926px 633px #fff, 1561px 1878px #fff,
      650px 1653px #fff, 2359px 990px #fff, 901px 166px #fff, 357px 753px #fff,
      962px 35px #fff, 2975px 1696px #fff, 1662px 1265px #fff, 302px 853px #fff,
      848px 983px #fff, 1260px 1384px #fff, 809px 1583px #fff, 961px 723px #fff,
      2991px 18px #fff, 799px 1371px #fff, 132px 1235px #fff, 981px 991px #fff,
      742px 535px #fff, 2880px 1988px #fff, 466px 738px #fff, 673px 703px #fff,
      11px 533px #fff, 2124px 158px #fff, 1652px 703px #fff, 818px 1315px #fff,
      2215px 1003px #fff, 1153px 1326px #fff, 234px 1816px #fff,
      1263px 656px #fff, 1025px 1396px #fff, 1140px 566px #fff,
      1684px 256px #fff, 2179px 1364px #fff, 1956px 80px #fff, 792px 602px #fff,
      1655px 1676px #fff, 2826px 584px #fff, 2853px 1656px #fff, 74px 232px #fff,
      911px 1983px #fff, 1835px 949px #fff, 2142px 1434px #fff,
      2403px 589px #fff, 672px 356px #fff, 1658px 371px #fff, 1727px 1392px #fff,
      2908px 1983px #fff, 969px 57px #fff, 149px 1225px #fff, 1384px 1256px #fff,
      1004px 152px #fff, 2436px 1725px #fff, 2418px 1009px #fff,
      1001px 574px #fff, 1949px 454px #fff, 2853px 1017px #fff,
      1591px 1667px #fff, 591px 1703px #fff, 1048px 511px #fff,
      129px 1409px #fff, 1909px 492px #fff, 84px 157px #fff, 984px 1137px #fff,
      42px 1001px #fff, 1473px 375px #fff, 2440px 1493px #fff, 425px 1069px #fff,
      862px 1343px #fff, 1577px 1641px #fff, 1557px 1820px #fff,
      1660px 1370px #fff, 651px 1441px #fff, 1086px 1307px #fff,
      1323px 1508px #fff, 1330px 1034px #fff, 1321px 341px #fff,
      575px 458px #fff, 1188px 1325px #fff, 2061px 838px #fff, 1335px 656px #fff,
      2847px 10px #fff, 520px 690px #fff, 1903px 1611px #fff, 1069px 1703px #fff,
      1544px 1487px #fff, 650px 1697px #fff, 409px 487px #fff, 289px 772px #fff,
      1424px 1636px #fff, 2631px 1337px #fff, 630px 1619px #fff,
      2349px 457px #fff, 2283px 1289px #fff, 933px 595px #fff, 1152px 237px #fff,
      888px 555px #fff, 2904px 327px #fff, 529px 1099px #fff, 2981px 1598px #fff,
      848px 759px #fff, 1778px 688px #fff, 2553px 969px #fff, 384px 465px #fff,
      2672px 645px #fff, 385px 1367px #fff, 1163px 1876px #fff, 204px 639px #fff,
      771px 1628px #fff, 785px 436px #fff, 2863px 1474px #fff, 722px 106px #fff,
      2464px 191px #fff, 17px 649px #fff, 1368px 957px #fff, 2645px 1634px #fff,
      2916px 1740px #fff, 1287px 384px #fff, 2994px 759px #fff,
      2885px 486px #fff, 1171px 1745px #fff, 1513px 507px #fff, 30px 972px #fff,
      2982px 1487px #fff, 2913px 1366px #fff, 2215px 1587px #fff,
      2810px 94px #fff, 2787px 1196px #fff, 1234px 1327px #fff,
      989px 1307px #fff, 1698px 662px #fff, 2098px 1479px #fff,
      2828px 1279px #fff, 421px 801px #fff, 2630px 625px #fff,
      1084px 1860px #fff, 1221px 293px #fff, 1648px 1547px #fff,
      2830px 647px #fff, 2604px 1939px #fff, 850px 1342px #fff, 846px 547px #fff,
      1976px 1846px #fff, 1981px 52px #fff, 1088px 1561px #fff,
      1507px 304px #fff, 827px 1548px #fff, 1866px 987px #fff, 362px 1486px #fff,
      2969px 166px #fff, 526px 1255px #fff, 773px 1835px #fff, 2713px 158px #fff,
      1526px 1305px #fff, 2677px 428px #fff, 1166px 257px #fff, 2572px 16px #fff,
      2190px 107px #fff, 1039px 1714px #fff, 904px 1323px #fff,
      1258px 1622px #fff, 1539px 1678px #fff, 689px 221px #fff,
      328px 1349px #fff, 1927px 1818px #fff, 880px 1800px #fff,
      177px 1172px #fff, 965px 1003px #fff, 347px 440px #fff, 901px 1922px #fff,
      2221px 1378px #fff, 732px 1312px #fff, 475px 1211px #fff,
      2543px 698px #fff, 2022px 687px #fff, 2px 1108px #fff, 1869px 698px #fff,
      6px 1509px #fff, 642px 1277px #fff, 1101px 1536px #fff, 2035px 755px #fff,
      1162px 1518px #fff, 2249px 258px #fff, 1319px 1093px #fff,
      1005px 1396px #fff, 1785px 1365px #fff, 2382px 431px #fff,
      2624px 1926px #fff, 2782px 1457px #fff, 2609px 1288px #fff,
      860px 1290px #fff, 2043px 538px #fff, 2257px 819px #fff, 1690px 76px #fff,
      812px 691px #fff, 1097px 68px #fff, 2301px 1885px #fff, 388px 812px #fff,
      656px 1511px #fff, 1488px 252px #fff, 2191px 1881px #fff,
      2307px 1515px #fff, 2036px 600px #fff, 569px 250px #fff, 2053px 863px #fff,
      364px 1314px #fff, 2195px 1614px #fff, 1959px 1739px #fff,
      1562px 175px #fff, 959px 705px #fff, 2483px 191px #fff, 671px 793px #fff,
      144px 709px #fff, 1714px 287px #fff, 840px 394px #fff, 200px 1629px #fff,
      1008px 1875px #fff, 501px 1711px #fff, 104px 440px #fff,
      2535px 1992px #fff, 2628px 1652px #fff, 2266px 617px #fff,
      2829px 1686px #fff, 1316px 1745px #fff, 1814px 366px #fff, 75px 175px #fff,
      13px 717px #fff, 2531px 1548px #fff, 295px 1944px #fff, 607px 1577px #fff,
      1111px 1135px #fff, 3000px 193px #fff, 2969px 734px #fff,
      2893px 231px #fff, 395px 1404px #fff, 602px 1464px #fff, 618px 353px #fff,
      2398px 1017px #fff, 2865px 1360px #fff, 2855px 65px #fff, 303px 951px #fff,
      2571px 751px #fff, 1777px 233px #fff, 791px 964px #fff, 2906px 533px #fff,
      540px 603px #fff, 2524px 1977px #fff, 2887px 803px #fff, 1466px 320px #fff,
      2208px 1699px #fff, 2180px 842px #fff, 2503px 718px #fff,
      2255px 481px #fff, 12px 327px #fff, 167px 295px #fff, 2778px 794px #fff,
      547px 1855px #fff, 1262px 618px #fff, 1600px 1454px #fff,
      2678px 1195px #fff, 2629px 1605px #fff, 2646px 659px #fff,
      285px 203px #fff, 2144px 334px #fff, 2057px 1246px #fff,
      2691px 1749px #fff, 2418px 1666px #fff, 669px 1247px #fff,
      2561px 1179px #fff, 1686px 829px #fff, 2118px 693px #fff,
      1692px 295px #fff, 1045px 965px #fff, 1315px 1209px #fff,
      2516px 1446px #fff, 2477px 1113px #fff, 2068px 1858px #fff,
      1480px 1919px #fff, 610px 1990px #fff, 1406px 1441px #fff,
      2470px 1001px #fff, 2611px 1850px #fff, 1647px 1876px #fff,
      906px 1318px #fff, 1360px 173px #fff, 1692px 221px #fff, 84px 863px #fff,
      1921px 1325px #fff, 508px 563px #fff, 1984px 1988px #fff, 97px 769px #fff,
      340px 878px #fff, 1926px 572px #fff, 2686px 1763px #fff, 210px 643px #fff,
      1984px 1709px #fff, 1445px 1455px #fff, 1219px 810px #fff,
      1014px 1463px #fff, 1503px 564px #fff, 287px 804px #fff, 864px 82px #fff,
      2580px 259px #fff, 1662px 75px #fff, 518px 450px #fff, 1917px 1050px #fff,
      399px 245px #fff, 1420px 627px #fff, 29px 669px #fff, 1820px 1013px #fff,
      2600px 734px #fff, 1233px 519px #fff, 191px 1308px #fff, 287px 1942px #fff,
      1315px 586px #fff, 1482px 1869px #fff, 459px 1934px #fff, 630px 186px #fff,
      182px 117px #fff, 128px 993px #fff, 897px 522px #fff, 678px 274px #fff,
      1215px 1981px #fff, 1998px 110px #fff, 2437px 1722px #fff,
      1909px 1101px #fff, 2993px 354px #fff, 502px 1409px #fff,
      1200px 284px #fff, 1032px 1836px #fff, 456px 441px #fff,
      2711px 1828px #fff, 459px 1419px #fff, 1961px 137px #fff,
      2814px 1131px #fff, 2117px 1720px #fff, 637px 96px #fff, 2582px 760px #fff,
      2337px 1627px #fff, 243px 406px #fff, 60px 822px #fff, 1960px 667px #fff,
      1821px 999px #fff, 1828px 885px #fff, 2432px 719px #fff, 2843px 50px #fff,
      947px 174px #fff, 1227px 298px #fff, 2904px 867px #fff, 397px 1445px #fff,
      157px 1165px #fff, 1059px 1009px #fff, 825px 1017px #fff,
      2805px 961px #fff, 412px 1260px #fff, 2958px 102px #fff, 275px 1296px #fff,
      2368px 317px #fff, 987px 1657px #fff, 1578px 1385px #fff,
      2483px 669px #fff, 444px 1552px #fff, 1413px 1053px #fff,
      1886px 583px #fff, 702px 396px #fff, 2782px 1729px #fff, 1834px 124px #fff,
      362px 1836px #fff, 2227px 1293px #fff, 533px 874px #fff,
      1948px 1139px #fff, 1999px 799px #fff, 1498px 1514px #fff, 1055px 9px #fff,
      162px 1514px #fff, 3000px 1649px #fff, 1240px 1536px #fff, 349px 27px #fff,
      1789px 994px #fff, 226px 262px #fff, 2419px 460px #fff, 2757px 60px #fff,
      652px 95px #fff, 2649px 1078px #fff, 691px 1916px #fff, 1627px 1244px #fff,
      2744px 1559px #fff, 1134px 1543px #fff, 1287px 1344px #fff,
      404px 204px #fff, 2878px 1423px #fff, 400px 98px #fff, 791px 953px #fff,
      2524px 593px #fff, 2762px 1658px #fff, 1904px 720px #fff, 6px 340px #fff,
      2519px 110px #fff, 1049px 1031px #fff, 970px 1256px #fff, 229px 49px #fff,
      2264px 861px #fff, 1670px 809px #fff, 2902px 1177px #fff,
      1224px 1159px #fff, 793px 812px #fff, 1034px 1704px #fff,
      1646px 1979px #fff, 691px 394px #fff, 1301px 1358px #fff,
      2465px 162px #fff, 337px 34px #fff, 808px 1687px #fff, 2005px 1305px #fff,
      836px 198px #fff, 249px 40px #fff, 2839px 939px #fff, 2393px 329px #fff,
      1588px 540px #fff, 872px 574px #fff, 1916px 1481px #fff,
      1921px 1660px #fff, 478px 464px #fff, 271px 410px #fff, 307px 1608px #fff,
      106px 1133px #fff, 485px 353px #fff, 982px 91px #fff, 742px 1860px #fff,
      2153px 646px #fff, 2882px 1232px #fff, 120px 1986px #fff,
      1360px 1069px #fff, 1114px 1396px #fff, 1460px 322px #fff,
      2754px 702px #fff, 2859px 902px #fff, 105px 1276px #fff, 1729px 650px #fff,
      315px 752px #fff, 770px 503px #fff, 2598px 867px #fff, 807px 1861px #fff,
      1065px 1300px #fff, 427px 1229px #fff, 294px 267px #fff,
      2218px 1322px #fff, 1409px 1012px #fff, 254px 304px #fff,
      2829px 1148px #fff, 2787px 567px #fff, 2835px 416px #fff,
      1847px 472px #fff, 1723px 1589px #fff, 1970px 315px #fff,
      2139px 571px #fff, 1098px 370px #fff, 1061px 1545px #fff, 893px 579px #fff,
      1852px 1367px #fff, 2279px 670px #fff, 1312px 5px #fff, 2977px 232px #fff,
      166px 139px #fff, 1725px 1411px #fff, 99px 767px #fff, 1898px 520px #fff,
      347px 1564px #fff, 2101px 693px #fff, 948px 1810px #fff,
      2202px 1547px #fff, 2340px 930px #fff, 1359px 181px #fff,
      2223px 669px #fff, 2235px 906px #fff, 62px 558px #fff, 336px 1526px #fff,
      462px 1997px #fff, 2414px 1659px #fff, 2231px 1875px #fff,
      1297px 267px #fff, 1957px 1325px #fff, 226px 528px #fff, 1710px 243px #fff,
      2920px 697px #fff, 2300px 1282px #fff, 605px 1791px #fff,
      852px 1123px #fff, 1827px 74px #fff, 1465px 1376px #fff, 1242px 130px #fff,
      974px 115px #fff, 757px 647px #fff, 2399px 600px #fff, 391px 52px #fff,
      38px 1702px #fff, 1166px 996px #fff, 2765px 1669px #fff,
      1644px 1751px #fff, 850px 496px #fff, 999px 38px #fff, 2528px 1282px #fff,
      246px 225px #fff, 2915px 1025px #fff, 203px 1670px #fff,
      1452px 1148px #fff, 1785px 765px #fff, 2185px 1382px #fff,
      2648px 405px #fff, 324px 155px #fff, 1246px 1736px #fff, 583px 1577px #fff,
      2437px 1567px #fff, 2233px 476px #fff, 2252px 1262px #fff,
      1253px 10px #fff, 812px 673px #fff, 2226px 356px #fff, 2870px 1110px #fff,
      2382px 1428px #fff, 1322px 1200px #fff, 106px 388px #fff,
      1137px 1620px #fff, 937px 203px #fff, 1424px 1381px #fff,
      2395px 1933px #fff, 1989px 856px #fff, 1457px 387px #fff,
      1322px 762px #fff, 800px 745px #fff, 2769px 1540px #fff, 38px 843px #fff,
      1820px 1456px #fff, 1413px 1981px #fff, 70px 824px #fff, 1118px 348px #fff,
      1253px 1810px #fff, 1709px 288px #fff, 1546px 1004px #fff,
      967px 1525px #fff, 2786px 436px #fff, 2441px 476px #fff, 348px 1891px #fff,
      100px 1649px #fff, 2371px 674px #fff, 2408px 559px #fff,
      2792px 1308px #fff, 2110px 1309px #fff, 2962px 723px #fff,
      2003px 241px #fff, 165px 263px #fff, 209px 934px #fff, 1657px 1409px #fff,
      1662px 790px #fff, 1195px 903px #fff, 1967px 1788px #fff,
      1803px 1787px #fff, 430px 1450px #fff, 1959px 1189px #fff,
      1343px 1735px #fff, 2326px 1955px #fff, 190px 1567px #fff,
      1816px 31px #fff, 1306px 659px #fff, 1526px 245px #fff, 784px 1850px #fff,
      1270px 146px #fff, 2844px 218px #fff, 1573px 1876px #fff,
      2236px 1002px #fff, 1470px 1145px #fff, 233px 957px #fff,
      1277px 544px #fff, 2217px 30px #fff, 1773px 616px #fff, 2181px 657px #fff,
      1711px 121px #fff, 1007px 510px #fff, 1391px 1716px #fff,
      2165px 875px #fff, 2458px 909px #fff, 1466px 194px #fff,
      1263px 1645px #fff, 1775px 663px #fff, 1399px 1647px #fff,
      785px 880px #fff, 2478px 922px #fff, 1050px 1613px #fff, 2687px 433px #fff,
      985px 533px #fff, 1168px 686px #fff, 242px 796px #fff, 1028px 1801px #fff,
      979px 361px #fff, 2923px 640px #fff, 140px 1664px #fff, 900px 1834px #fff,
      1045px 202px #fff, 48px 1121px #fff;
    animation: animStar 50s linear infinite;
  }
  #stars:after {
    content: " ";
    position: absolute;
    top: 2000px;
    width: 1px;
    height: 1px;
    background: transparent;
    box-shadow: 1165px 1064px #fff, 326px 1081px #fff, 1613px 1333px #fff,
      2150px 623px #fff, 1149px 915px #fff, 1796px 696px #fff, 928px 1206px #fff,
      2435px 1880px #fff, 2794px 1606px #fff, 876px 827px #fff,
      2654px 704px #fff, 295px 655px #fff, 93px 1612px #fff, 2316px 399px #fff,
      1898px 1462px #fff, 530px 579px #fff, 2006px 1003px #fff,
      2185px 1668px #fff, 2141px 693px #fff, 2926px 529px #fff,
      1529px 546px #fff, 1583px 432px #fff, 1365px 594px #fff, 2551px 564px #fff,
      404px 400px #fff, 1322px 115px #fff, 633px 1943px #fff, 1149px 765px #fff,
      1117px 566px #fff, 1353px 544px #fff, 2676px 320px #fff, 1259px 857px #fff,
      446px 1731px #fff, 60px 142px #fff, 15px 758px #fff, 1031px 1329px #fff,
      2513px 1821px #fff, 338px 1104px #fff, 2179px 1677px #fff,
      1793px 407px #fff, 1782px 989px #fff, 2607px 1009px #fff,
      1446px 1113px #fff, 1151px 1498px #fff, 539px 1235px #fff,
      2157px 317px #fff, 2579px 1571px #fff, 1036px 620px #fff,
      1031px 435px #fff, 504px 412px #fff, 2036px 1456px #fff,
      2157px 1597px #fff, 815px 60px #fff, 2895px 42px #fff, 1222px 530px #fff,
      2978px 263px #fff, 2673px 1905px #fff, 2029px 1168px #fff,
      2143px 565px #fff, 2460px 165px #fff, 527px 1381px #fff,
      1624px 1573px #fff, 2894px 1304px #fff, 864px 1996px #fff,
      1705px 485px #fff, 2079px 659px #fff, 714px 1728px #fff,
      2436px 1104px #fff, 2821px 878px #fff, 1794px 367px #fff,
      1528px 1114px #fff, 779px 1106px #fff, 448px 1058px #fff, 611px 401px #fff,
      1936px 1449px #fff, 2691px 1019px #fff, 1029px 1223px #fff,
      2693px 1909px #fff, 1393px 1292px #fff, 71px 816px #fff,
      1259px 1219px #fff, 28px 1422px #fff, 844px 744px #fff, 1015px 941px #fff,
      2048px 383px #fff, 2376px 369px #fff, 2771px 660px #fff,
      2688px 1747px #fff, 1240px 1339px #fff, 2408px 139px #fff,
      2545px 859px #fff, 2773px 1758px #fff, 2580px 163px #fff,
      2211px 789px #fff, 2285px 42px #fff, 53px 1699px #fff, 729px 181px #fff,
      524px 1451px #fff, 1809px 520px #fff, 2529px 1791px #fff,
      2791px 1809px #fff, 926px 633px #fff, 1561px 1878px #fff,
      650px 1653px #fff, 2359px 990px #fff, 901px 166px #fff, 357px 753px #fff,
      962px 35px #fff, 2975px 1696px #fff, 1662px 1265px #fff, 302px 853px #fff,
      848px 983px #fff, 1260px 1384px #fff, 809px 1583px #fff, 961px 723px #fff,
      2991px 18px #fff, 799px 1371px #fff, 132px 1235px #fff, 981px 991px #fff,
      742px 535px #fff, 2880px 1988px #fff, 466px 738px #fff, 673px 703px #fff,
      11px 533px #fff, 2124px 158px #fff, 1652px 703px #fff, 818px 1315px #fff,
      2215px 1003px #fff, 1153px 1326px #fff, 234px 1816px #fff,
      1263px 656px #fff, 1025px 1396px #fff, 1140px 566px #fff,
      1684px 256px #fff, 2179px 1364px #fff, 1956px 80px #fff, 792px 602px #fff,
      1655px 1676px #fff, 2826px 584px #fff, 2853px 1656px #fff, 74px 232px #fff,
      911px 1983px #fff, 1835px 949px #fff, 2142px 1434px #fff,
      2403px 589px #fff, 672px 356px #fff, 1658px 371px #fff, 1727px 1392px #fff,
      2908px 1983px #fff, 969px 57px #fff, 149px 1225px #fff, 1384px 1256px #fff,
      1004px 152px #fff, 2436px 1725px #fff, 2418px 1009px #fff,
      1001px 574px #fff, 1949px 454px #fff, 2853px 1017px #fff,
      1591px 1667px #fff, 591px 1703px #fff, 1048px 511px #fff,
      129px 1409px #fff, 1909px 492px #fff, 84px 157px #fff, 984px 1137px #fff,
      42px 1001px #fff, 1473px 375px #fff, 2440px 1493px #fff, 425px 1069px #fff,
      862px 1343px #fff, 1577px 1641px #fff, 1557px 1820px #fff,
      1660px 1370px #fff, 651px 1441px #fff, 1086px 1307px #fff,
      1323px 1508px #fff, 1330px 1034px #fff, 1321px 341px #fff,
      575px 458px #fff, 1188px 1325px #fff, 2061px 838px #fff, 1335px 656px #fff,
      2847px 10px #fff, 520px 690px #fff, 1903px 1611px #fff, 1069px 1703px #fff,
      1544px 1487px #fff, 650px 1697px #fff, 409px 487px #fff, 289px 772px #fff,
      1424px 1636px #fff, 2631px 1337px #fff, 630px 1619px #fff,
      2349px 457px #fff, 2283px 1289px #fff, 933px 595px #fff, 1152px 237px #fff,
      888px 555px #fff, 2904px 327px #fff, 529px 1099px #fff, 2981px 1598px #fff,
      848px 759px #fff, 1778px 688px #fff, 2553px 969px #fff, 384px 465px #fff,
      2672px 645px #fff, 385px 1367px #fff, 1163px 1876px #fff, 204px 639px #fff,
      771px 1628px #fff, 785px 436px #fff, 2863px 1474px #fff, 722px 106px #fff,
      2464px 191px #fff, 17px 649px #fff, 1368px 957px #fff, 2645px 1634px #fff,
      2916px 1740px #fff, 1287px 384px #fff, 2994px 759px #fff,
      2885px 486px #fff, 1171px 1745px #fff, 1513px 507px #fff, 30px 972px #fff,
      2982px 1487px #fff, 2913px 1366px #fff, 2215px 1587px #fff,
      2810px 94px #fff, 2787px 1196px #fff, 1234px 1327px #fff,
      989px 1307px #fff, 1698px 662px #fff, 2098px 1479px #fff,
      2828px 1279px #fff, 421px 801px #fff, 2630px 625px #fff,
      1084px 1860px #fff, 1221px 293px #fff, 1648px 1547px #fff,
      2830px 647px #fff, 2604px 1939px #fff, 850px 1342px #fff, 846px 547px #fff,
      1976px 1846px #fff, 1981px 52px #fff, 1088px 1561px #fff,
      1507px 304px #fff, 827px 1548px #fff, 1866px 987px #fff, 362px 1486px #fff,
      2969px 166px #fff, 526px 1255px #fff, 773px 1835px #fff, 2713px 158px #fff,
      1526px 1305px #fff, 2677px 428px #fff, 1166px 257px #fff, 2572px 16px #fff,
      2190px 107px #fff, 1039px 1714px #fff, 904px 1323px #fff,
      1258px 1622px #fff, 1539px 1678px #fff, 689px 221px #fff,
      328px 1349px #fff, 1927px 1818px #fff, 880px 1800px #fff,
      177px 1172px #fff, 965px 1003px #fff, 347px 440px #fff, 901px 1922px #fff,
      2221px 1378px #fff, 732px 1312px #fff, 475px 1211px #fff,
      2543px 698px #fff, 2022px 687px #fff, 2px 1108px #fff, 1869px 698px #fff,
      6px 1509px #fff, 642px 1277px #fff, 1101px 1536px #fff, 2035px 755px #fff,
      1162px 1518px #fff, 2249px 258px #fff, 1319px 1093px #fff,
      1005px 1396px #fff, 1785px 1365px #fff, 2382px 431px #fff,
      2624px 1926px #fff, 2782px 1457px #fff, 2609px 1288px #fff,
      860px 1290px #fff, 2043px 538px #fff, 2257px 819px #fff, 1690px 76px #fff,
      812px 691px #fff, 1097px 68px #fff, 2301px 1885px #fff, 388px 812px #fff,
      656px 1511px #fff, 1488px 252px #fff, 2191px 1881px #fff,
      2307px 1515px #fff, 2036px 600px #fff, 569px 250px #fff, 2053px 863px #fff,
      364px 1314px #fff, 2195px 1614px #fff, 1959px 1739px #fff,
      1562px 175px #fff, 959px 705px #fff, 2483px 191px #fff, 671px 793px #fff,
      144px 709px #fff, 1714px 287px #fff, 840px 394px #fff, 200px 1629px #fff,
      1008px 1875px #fff, 501px 1711px #fff, 104px 440px #fff,
      2535px 1992px #fff, 2628px 1652px #fff, 2266px 617px #fff,
      2829px 1686px #fff, 1316px 1745px #fff, 1814px 366px #fff, 75px 175px #fff,
      13px 717px #fff, 2531px 1548px #fff, 295px 1944px #fff, 607px 1577px #fff,
      1111px 1135px #fff, 3000px 193px #fff, 2969px 734px #fff,
      2893px 231px #fff, 395px 1404px #fff, 602px 1464px #fff, 618px 353px #fff,
      2398px 1017px #fff, 2865px 1360px #fff, 2855px 65px #fff, 303px 951px #fff,
      2571px 751px #fff, 1777px 233px #fff, 791px 964px #fff, 2906px 533px #fff,
      540px 603px #fff, 2524px 1977px #fff, 2887px 803px #fff, 1466px 320px #fff,
      2208px 1699px #fff, 2180px 842px #fff, 2503px 718px #fff,
      2255px 481px #fff, 12px 327px #fff, 167px 295px #fff, 2778px 794px #fff,
      547px 1855px #fff, 1262px 618px #fff, 1600px 1454px #fff,
      2678px 1195px #fff, 2629px 1605px #fff, 2646px 659px #fff,
      285px 203px #fff, 2144px 334px #fff, 2057px 1246px #fff,
      2691px 1749px #fff, 2418px 1666px #fff, 669px 1247px #fff,
      2561px 1179px #fff, 1686px 829px #fff, 2118px 693px #fff,
      1692px 295px #fff, 1045px 965px #fff, 1315px 1209px #fff,
      2516px 1446px #fff, 2477px 1113px #fff, 2068px 1858px #fff,
      1480px 1919px #fff, 610px 1990px #fff, 1406px 1441px #fff,
      2470px 1001px #fff, 2611px 1850px #fff, 1647px 1876px #fff,
      906px 1318px #fff, 1360px 173px #fff, 1692px 221px #fff, 84px 863px #fff,
      1921px 1325px #fff, 508px 563px #fff, 1984px 1988px #fff, 97px 769px #fff,
      340px 878px #fff, 1926px 572px #fff, 2686px 1763px #fff, 210px 643px #fff,
      1984px 1709px #fff, 1445px 1455px #fff, 1219px 810px #fff,
      1014px 1463px #fff, 1503px 564px #fff, 287px 804px #fff, 864px 82px #fff,
      2580px 259px #fff, 1662px 75px #fff, 518px 450px #fff, 1917px 1050px #fff,
      399px 245px #fff, 1420px 627px #fff, 29px 669px #fff, 1820px 1013px #fff,
      2600px 734px #fff, 1233px 519px #fff, 191px 1308px #fff, 287px 1942px #fff,
      1315px 586px #fff, 1482px 1869px #fff, 459px 1934px #fff, 630px 186px #fff,
      182px 117px #fff, 128px 993px #fff, 897px 522px #fff, 678px 274px #fff,
      1215px 1981px #fff, 1998px 110px #fff, 2437px 1722px #fff,
      1909px 1101px #fff, 2993px 354px #fff, 502px 1409px #fff,
      1200px 284px #fff, 1032px 1836px #fff, 456px 441px #fff,
      2711px 1828px #fff, 459px 1419px #fff, 1961px 137px #fff,
      2814px 1131px #fff, 2117px 1720px #fff, 637px 96px #fff, 2582px 760px #fff,
      2337px 1627px #fff, 243px 406px #fff, 60px 822px #fff, 1960px 667px #fff,
      1821px 999px #fff, 1828px 885px #fff, 2432px 719px #fff, 2843px 50px #fff,
      947px 174px #fff, 1227px 298px #fff, 2904px 867px #fff, 397px 1445px #fff,
      157px 1165px #fff, 1059px 1009px #fff, 825px 1017px #fff,
      2805px 961px #fff, 412px 1260px #fff, 2958px 102px #fff, 275px 1296px #fff,
      2368px 317px #fff, 987px 1657px #fff, 1578px 1385px #fff,
      2483px 669px #fff, 444px 1552px #fff, 1413px 1053px #fff,
      1886px 583px #fff, 702px 396px #fff, 2782px 1729px #fff, 1834px 124px #fff,
      362px 1836px #fff, 2227px 1293px #fff, 533px 874px #fff,
      1948px 1139px #fff, 1999px 799px #fff, 1498px 1514px #fff, 1055px 9px #fff,
      162px 1514px #fff, 3000px 1649px #fff, 1240px 1536px #fff, 349px 27px #fff,
      1789px 994px #fff, 226px 262px #fff, 2419px 460px #fff, 2757px 60px #fff,
      652px 95px #fff, 2649px 1078px #fff, 691px 1916px #fff, 1627px 1244px #fff,
      2744px 1559px #fff, 1134px 1543px #fff, 1287px 1344px #fff,
      404px 204px #fff, 2878px 1423px #fff, 400px 98px #fff, 791px 953px #fff,
      2524px 593px #fff, 2762px 1658px #fff, 1904px 720px #fff, 6px 340px #fff,
      2519px 110px #fff, 1049px 1031px #fff, 970px 1256px #fff, 229px 49px #fff,
      2264px 861px #fff, 1670px 809px #fff, 2902px 1177px #fff,
      1224px 1159px #fff, 793px 812px #fff, 1034px 1704px #fff,
      1646px 1979px #fff, 691px 394px #fff, 1301px 1358px #fff,
      2465px 162px #fff, 337px 34px #fff, 808px 1687px #fff, 2005px 1305px #fff,
      836px 198px #fff, 249px 40px #fff, 2839px 939px #fff, 2393px 329px #fff,
      1588px 540px #fff, 872px 574px #fff, 1916px 1481px #fff,
      1921px 1660px #fff, 478px 464px #fff, 271px 410px #fff, 307px 1608px #fff,
      106px 1133px #fff, 485px 353px #fff, 982px 91px #fff, 742px 1860px #fff,
      2153px 646px #fff, 2882px 1232px #fff, 120px 1986px #fff,
      1360px 1069px #fff, 1114px 1396px #fff, 1460px 322px #fff,
      2754px 702px #fff, 2859px 902px #fff, 105px 1276px #fff, 1729px 650px #fff,
      315px 752px #fff, 770px 503px #fff, 2598px 867px #fff, 807px 1861px #fff,
      1065px 1300px #fff, 427px 1229px #fff, 294px 267px #fff,
      2218px 1322px #fff, 1409px 1012px #fff, 254px 304px #fff,
      2829px 1148px #fff, 2787px 567px #fff, 2835px 416px #fff,
      1847px 472px #fff, 1723px 1589px #fff, 1970px 315px #fff,
      2139px 571px #fff, 1098px 370px #fff, 1061px 1545px #fff, 893px 579px #fff,
      1852px 1367px #fff, 2279px 670px #fff, 1312px 5px #fff, 2977px 232px #fff,
      166px 139px #fff, 1725px 1411px #fff, 99px 767px #fff, 1898px 520px #fff,
      347px 1564px #fff, 2101px 693px #fff, 948px 1810px #fff,
      2202px 1547px #fff, 2340px 930px #fff, 1359px 181px #fff,
      2223px 669px #fff, 2235px 906px #fff, 62px 558px #fff, 336px 1526px #fff,
      462px 1997px #fff, 2414px 1659px #fff, 2231px 1875px #fff,
      1297px 267px #fff, 1957px 1325px #fff, 226px 528px #fff, 1710px 243px #fff,
      2920px 697px #fff, 2300px 1282px #fff, 605px 1791px #fff,
      852px 1123px #fff, 1827px 74px #fff, 1465px 1376px #fff, 1242px 130px #fff,
      974px 115px #fff, 757px 647px #fff, 2399px 600px #fff, 391px 52px #fff,
      38px 1702px #fff, 1166px 996px #fff, 2765px 1669px #fff,
      1644px 1751px #fff, 850px 496px #fff, 999px 38px #fff, 2528px 1282px #fff,
      246px 225px #fff, 2915px 1025px #fff, 203px 1670px #fff,
      1452px 1148px #fff, 1785px 765px #fff, 2185px 1382px #fff,
      2648px 405px #fff, 324px 155px #fff, 1246px 1736px #fff, 583px 1577px #fff,
      2437px 1567px #fff, 2233px 476px #fff, 2252px 1262px #fff,
      1253px 10px #fff, 812px 673px #fff, 2226px 356px #fff, 2870px 1110px #fff,
      2382px 1428px #fff, 1322px 1200px #fff, 106px 388px #fff,
      1137px 1620px #fff, 937px 203px #fff, 1424px 1381px #fff,
      2395px 1933px #fff, 1989px 856px #fff, 1457px 387px #fff,
      1322px 762px #fff, 800px 745px #fff, 2769px 1540px #fff, 38px 843px #fff,
      1820px 1456px #fff, 1413px 1981px #fff, 70px 824px #fff, 1118px 348px #fff,
      1253px 1810px #fff, 1709px 288px #fff, 1546px 1004px #fff,
      967px 1525px #fff, 2786px 436px #fff, 2441px 476px #fff, 348px 1891px #fff,
      100px 1649px #fff, 2371px 674px #fff, 2408px 559px #fff,
      2792px 1308px #fff, 2110px 1309px #fff, 2962px 723px #fff,
      2003px 241px #fff, 165px 263px #fff, 209px 934px #fff, 1657px 1409px #fff,
      1662px 790px #fff, 1195px 903px #fff, 1967px 1788px #fff,
      1803px 1787px #fff, 430px 1450px #fff, 1959px 1189px #fff,
      1343px 1735px #fff, 2326px 1955px #fff, 190px 1567px #fff,
      1816px 31px #fff, 1306px 659px #fff, 1526px 245px #fff, 784px 1850px #fff,
      1270px 146px #fff, 2844px 218px #fff, 1573px 1876px #fff,
      2236px 1002px #fff, 1470px 1145px #fff, 233px 957px #fff,
      1277px 544px #fff, 2217px 30px #fff, 1773px 616px #fff, 2181px 657px #fff,
      1711px 121px #fff, 1007px 510px #fff, 1391px 1716px #fff,
      2165px 875px #fff, 2458px 909px #fff, 1466px 194px #fff,
      1263px 1645px #fff, 1775px 663px #fff, 1399px 1647px #fff,
      785px 880px #fff, 2478px 922px #fff, 1050px 1613px #fff, 2687px 433px #fff,
      985px 533px #fff, 1168px 686px #fff, 242px 796px #fff, 1028px 1801px #fff,
      979px 361px #fff, 2923px 640px #fff, 140px 1664px #fff, 900px 1834px #fff,
      1045px 202px #fff, 48px 1121px #fff;
  }
  #stars2 {
    width: 2px;
    height: 2px;
    background: transparent;
    box-shadow: 1340px 1871px #fff, 2749px 773px #fff, 110px 1155px #fff,
      2364px 1707px #fff, 1154px 1212px #fff, 2309px 419px #fff,
      1034px 1948px #fff, 1521px 1664px #fff, 2325px 1052px #fff,
      1000px 1414px #fff, 793px 674px #fff, 647px 1830px #fff, 2339px 123px #fff,
      78px 1528px #fff, 2520px 401px #fff, 571px 1231px #fff, 1840px 1047px #fff,
      669px 252px #fff, 1883px 806px #fff, 1773px 65px #fff, 2601px 546px #fff,
      873px 1780px #fff, 2427px 1248px #fff, 81px 367px #fff, 2067px 486px #fff,
      1265px 263px #fff, 2764px 1871px #fff, 1512px 910px #fff,
      1246px 413px #fff, 23px 1740px #fff, 1896px 1059px #fff, 2833px 240px #fff,
      1886px 367px #fff, 1827px 1878px #fff, 2571px 1255px #fff,
      225px 1665px #fff, 1967px 1133px #fff, 1083px 1244px #fff,
      2274px 164px #fff, 2809px 1336px #fff, 622px 1610px #fff,
      1914px 588px #fff, 1113px 552px #fff, 219px 504px #fff, 1330px 1992px #fff,
      693px 129px #fff, 1943px 230px #fff, 2392px 116px #fff, 166px 346px #fff,
      131px 980px #fff, 2656px 1095px #fff, 1063px 749px #fff, 709px 541px #fff,
      469px 621px #fff, 2448px 1109px #fff, 1790px 1164px #fff, 2859px 69px #fff,
      368px 259px #fff, 2933px 1401px #fff, 739px 1410px #fff,
      2981px 1714px #fff, 1423px 1686px #fff, 1878px 833px #fff,
      121px 1982px #fff, 2640px 119px #fff, 2407px 1820px #fff,
      1205px 343px #fff, 2609px 1424px #fff, 1173px 1914px #fff,
      2503px 1968px #fff, 179px 1485px #fff, 504px 618px #fff,
      2861px 1253px #fff, 2150px 1833px #fff, 1163px 1518px #fff,
      1231px 836px #fff, 937px 1715px #fff, 1941px 846px #fff, 2552px 16px #fff,
      691px 1743px #fff, 651px 1277px #fff, 362px 1149px #fff, 1067px 834px #fff,
      135px 855px #fff, 1176px 723px #fff, 2324px 1406px #fff, 1847px 36px #fff,
      564px 750px #fff, 239px 1219px #fff, 1878px 1669px #fff, 471px 1473px #fff,
      2989px 1100px #fff, 1484px 1085px #fff, 511px 1364px #fff,
      2015px 1140px #fff, 2518px 350px #fff, 2500px 552px #fff, 906px 162px #fff,
      723px 926px #fff, 1707px 1546px #fff, 505px 1122px #fff, 1636px 512px #fff,
      1685px 1541px #fff, 695px 1930px #fff, 1534px 78px #fff, 153px 765px #fff,
      1090px 1063px #fff, 23px 1887px #fff, 11px 1417px #fff, 2351px 57px #fff,
      1736px 701px #fff, 1896px 457px #fff, 1809px 1903px #fff,
      1373px 1114px #fff, 2270px 640px #fff, 336px 1147px #fff, 364px 479px #fff,
      1463px 294px #fff, 2272px 76px #fff, 476px 1800px #fff, 65px 1335px #fff,
      2416px 888px #fff, 1411px 1871px #fff, 2219px 1716px #fff,
      2490px 998px #fff, 1804px 1812px #fff, 2129px 894px #fff,
      2709px 837px #fff, 116px 1214px #fff, 292px 781px #fff, 2179px 801px #fff,
      2710px 1987px #fff, 2420px 1487px #fff, 1002px 38px #fff,
      1183px 1551px #fff, 2378px 207px #fff, 107px 1919px #fff,
      1537px 882px #fff, 100px 1915px #fff, 1892px 1337px #fff,
      2102px 536px #fff, 2748px 488px #fff, 2589px 1136px #fff,
      1116px 1655px #fff, 231px 197px #fff, 1129px 1073px #fff,
      478px 1528px #fff, 2947px 1202px #fff, 2479px 1978px #fff,
      2566px 1068px #fff, 1152px 1734px #fff, 245px 81px #fff, 1559px 167px #fff,
      1299px 1738px #fff, 1509px 220px #fff, 2948px 853px #fff,
      1868px 1094px #fff, 1971px 772px #fff, 1553px 676px #fff,
      2998px 1539px #fff, 1320px 848px #fff, 2048px 1424px #fff,
      590px 1009px #fff, 1419px 819px #fff, 2503px 1568px #fff, 1382px 68px #fff,
      2630px 1293px #fff, 936px 1602px #fff, 508px 676px #fff, 674px 606px #fff,
      201px 66px #fff, 2114px 951px #fff, 2078px 1327px #fff, 1497px 1982px #fff,
      1440px 1400px #fff, 2910px 254px #fff, 2764px 963px #fff,
      1536px 1255px #fff, 2162px 378px #fff, 2033px 1766px #fff,
      2691px 716px #fff, 1533px 329px #fff, 1070px 1881px #fff, 224px 691px #fff,
      1436px 796px #fff, 444px 232px #fff, 1659px 734px #fff, 2946px 957px #fff,
      940px 1973px #fff, 2931px 820px #fff, 2651px 940px #fff, 2722px 674px #fff,
      2648px 24px #fff, 2972px 295px #fff, 1575px 783px #fff, 2236px 1191px #fff,
      2652px 1762px #fff, 1183px 538px #fff, 2913px 1925px #fff,
      1747px 661px #fff;
    animation: animStar 100s linear infinite;
  }
  #stars2:after {
    content: " ";
    position: absolute;
    top: 2000px;
    width: 2px;
    height: 2px;
    background: transparent;
    box-shadow: 1340px 1871px #fff, 2749px 773px #fff, 110px 1155px #fff,
      2364px 1707px #fff, 1154px 1212px #fff, 2309px 419px #fff,
      1034px 1948px #fff, 1521px 1664px #fff, 2325px 1052px #fff,
      1000px 1414px #fff, 793px 674px #fff, 647px 1830px #fff, 2339px 123px #fff,
      78px 1528px #fff, 2520px 401px #fff, 571px 1231px #fff, 1840px 1047px #fff,
      669px 252px #fff, 1883px 806px #fff, 1773px 65px #fff, 2601px 546px #fff,
      873px 1780px #fff, 2427px 1248px #fff, 81px 367px #fff, 2067px 486px #fff,
      1265px 263px #fff, 2764px 1871px #fff, 1512px 910px #fff,
      1246px 413px #fff, 23px 1740px #fff, 1896px 1059px #fff, 2833px 240px #fff,
      1886px 367px #fff, 1827px 1878px #fff, 2571px 1255px #fff,
      225px 1665px #fff, 1967px 1133px #fff, 1083px 1244px #fff,
      2274px 164px #fff, 2809px 1336px #fff, 622px 1610px #fff,
      1914px 588px #fff, 1113px 552px #fff, 219px 504px #fff, 1330px 1992px #fff,
      693px 129px #fff, 1943px 230px #fff, 2392px 116px #fff, 166px 346px #fff,
      131px 980px #fff, 2656px 1095px #fff, 1063px 749px #fff, 709px 541px #fff,
      469px 621px #fff, 2448px 1109px #fff, 1790px 1164px #fff, 2859px 69px #fff,
      368px 259px #fff, 2933px 1401px #fff, 739px 1410px #fff,
      2981px 1714px #fff, 1423px 1686px #fff, 1878px 833px #fff,
      121px 1982px #fff, 2640px 119px #fff, 2407px 1820px #fff,
      1205px 343px #fff, 2609px 1424px #fff, 1173px 1914px #fff,
      2503px 1968px #fff, 179px 1485px #fff, 504px 618px #fff,
      2861px 1253px #fff, 2150px 1833px #fff, 1163px 1518px #fff,
      1231px 836px #fff, 937px 1715px #fff, 1941px 846px #fff, 2552px 16px #fff,
      691px 1743px #fff, 651px 1277px #fff, 362px 1149px #fff, 1067px 834px #fff,
      135px 855px #fff, 1176px 723px #fff, 2324px 1406px #fff, 1847px 36px #fff,
      564px 750px #fff, 239px 1219px #fff, 1878px 1669px #fff, 471px 1473px #fff,
      2989px 1100px #fff, 1484px 1085px #fff, 511px 1364px #fff,
      2015px 1140px #fff, 2518px 350px #fff, 2500px 552px #fff, 906px 162px #fff,
      723px 926px #fff, 1707px 1546px #fff, 505px 1122px #fff, 1636px 512px #fff,
      1685px 1541px #fff, 695px 1930px #fff, 1534px 78px #fff, 153px 765px #fff,
      1090px 1063px #fff, 23px 1887px #fff, 11px 1417px #fff, 2351px 57px #fff,
      1736px 701px #fff, 1896px 457px #fff, 1809px 1903px #fff,
      1373px 1114px #fff, 2270px 640px #fff, 336px 1147px #fff, 364px 479px #fff,
      1463px 294px #fff, 2272px 76px #fff, 476px 1800px #fff, 65px 1335px #fff,
      2416px 888px #fff, 1411px 1871px #fff, 2219px 1716px #fff,
      2490px 998px #fff, 1804px 1812px #fff, 2129px 894px #fff,
      2709px 837px #fff, 116px 1214px #fff, 292px 781px #fff, 2179px 801px #fff,
      2710px 1987px #fff, 2420px 1487px #fff, 1002px 38px #fff,
      1183px 1551px #fff, 2378px 207px #fff, 107px 1919px #fff,
      1537px 882px #fff, 100px 1915px #fff, 1892px 1337px #fff,
      2102px 536px #fff, 2748px 488px #fff, 2589px 1136px #fff,
      1116px 1655px #fff, 231px 197px #fff, 1129px 1073px #fff,
      478px 1528px #fff, 2947px 1202px #fff, 2479px 1978px #fff,
      2566px 1068px #fff, 1152px 1734px #fff, 245px 81px #fff, 1559px 167px #fff,
      1299px 1738px #fff, 1509px 220px #fff, 2948px 853px #fff,
      1868px 1094px #fff, 1971px 772px #fff, 1553px 676px #fff,
      2998px 1539px #fff, 1320px 848px #fff, 2048px 1424px #fff,
      590px 1009px #fff, 1419px 819px #fff, 2503px 1568px #fff, 1382px 68px #fff,
      2630px 1293px #fff, 936px 1602px #fff, 508px 676px #fff, 674px 606px #fff,
      201px 66px #fff, 2114px 951px #fff, 2078px 1327px #fff, 1497px 1982px #fff,
      1440px 1400px #fff, 2910px 254px #fff, 2764px 963px #fff,
      1536px 1255px #fff, 2162px 378px #fff, 2033px 1766px #fff,
      2691px 716px #fff, 1533px 329px #fff, 1070px 1881px #fff, 224px 691px #fff,
      1436px 796px #fff, 444px 232px #fff, 1659px 734px #fff, 2946px 957px #fff,
      940px 1973px #fff, 2931px 820px #fff, 2651px 940px #fff, 2722px 674px #fff,
      2648px 24px #fff, 2972px 295px #fff, 1575px 783px #fff, 2236px 1191px #fff,
      2652px 1762px #fff, 1183px 538px #fff, 2913px 1925px #fff,
      1747px 661px #fff;
  }
  #stars3 {
    width: 3px;
    height: 3px;
    background: transparent;
    box-shadow: 1508px 1869px #fff, 265px 1203px #fff, 2359px 136px #fff,
      650px 599px #fff, 1537px 1721px #fff, 2955px 378px #fff,
      1676px 1074px #fff, 25px 489px #fff, 2171px 1845px #fff, 513px 558px #fff,
      1844px 1505px #fff, 2220px 673px #fff, 1791px 1996px #fff,
      2341px 981px #fff, 1473px 1687px #fff, 2956px 542px #fff,
      1287px 399px #fff, 1336px 511px #fff, 2358px 975px #fff, 2807px 140px #fff,
      1291px 31px #fff, 2627px 393px #fff, 2473px 1382px #fff, 333px 1240px #fff,
      87px 1538px #fff, 2372px 175px #fff, 419px 1512px #fff, 2993px 1711px #fff,
      2204px 1394px #fff, 2043px 1610px #fff, 2717px 640px #fff,
      2256px 339px #fff, 2305px 1998px #fff, 2783px 504px #fff,
      1510px 593px #fff, 1369px 1498px #fff, 101px 1051px #fff,
      564px 1300px #fff, 2000px 1445px #fff, 2640px 506px #fff,
      2970px 1318px #fff, 1326px 1863px #fff, 590px 97px #fff,
      2380px 1393px #fff, 1158px 1423px #fff, 1753px 1891px #fff,
      2606px 1455px #fff, 953px 1009px #fff, 2277px 413px #fff,
      2863px 744px #fff, 497px 1229px #fff, 298px 264px #fff, 2311px 600px #fff,
      1557px 1253px #fff, 87px 673px #fff, 401px 1682px #fff, 2450px 1438px #fff,
      1902px 1515px #fff, 1631px 1883px #fff, 687px 671px #fff,
      2478px 1712px #fff, 2470px 920px #fff, 632px 1818px #fff, 89px 1594px #fff,
      2043px 1140px #fff, 63px 688px #fff, 1726px 922px #fff, 2076px 1909px #fff,
      2234px 419px #fff, 476px 1730px #fff, 1053px 1137px #fff,
      1448px 1853px #fff, 173px 759px #fff, 2986px 40px #fff, 2778px 1837px #fff,
      2874px 1221px #fff, 572px 1338px #fff, 2014px 672px #fff, 984px 923px #fff,
      2277px 1656px #fff, 1602px 662px #fff, 1624px 1980px #fff,
      2378px 621px #fff, 1077px 1086px #fff, 724px 1275px #fff, 873px 529px #fff,
      821px 2000px #fff, 2917px 882px #fff, 1468px 919px #fff, 825px 1290px #fff,
      1810px 1693px #fff, 193px 353px #fff, 1262px 631px #fff, 2572px 154px #fff,
      1012px 431px #fff, 1787px 895px #fff, 129px 1395px #fff,
      2194px 1513px #fff, 415px 1678px #fff, 2698px 761px #fff;
    animation: animStar 150s linear infinite;
  }
  #stars3:after {
    content: " ";
    position: absolute;
    top: 2000px;
    width: 3px;
    height: 3px;
    background: transparent;
    box-shadow: 1508px 1869px #fff, 265px 1203px #fff, 2359px 136px #fff,
      650px 599px #fff, 1537px 1721px #fff, 2955px 378px #fff,
      1676px 1074px #fff, 25px 489px #fff, 2171px 1845px #fff, 513px 558px #fff,
      1844px 1505px #fff, 2220px 673px #fff, 1791px 1996px #fff,
      2341px 981px #fff, 1473px 1687px #fff, 2956px 542px #fff,
      1287px 399px #fff, 1336px 511px #fff, 2358px 975px #fff, 2807px 140px #fff,
      1291px 31px #fff, 2627px 393px #fff, 2473px 1382px #fff, 333px 1240px #fff,
      87px 1538px #fff, 2372px 175px #fff, 419px 1512px #fff, 2993px 1711px #fff,
      2204px 1394px #fff, 2043px 1610px #fff, 2717px 640px #fff,
      2256px 339px #fff, 2305px 1998px #fff, 2783px 504px #fff,
      1510px 593px #fff, 1369px 1498px #fff, 101px 1051px #fff,
      564px 1300px #fff, 2000px 1445px #fff, 2640px 506px #fff,
      2970px 1318px #fff, 1326px 1863px #fff, 590px 97px #fff,
      2380px 1393px #fff, 1158px 1423px #fff, 1753px 1891px #fff,
      2606px 1455px #fff, 953px 1009px #fff, 2277px 413px #fff,
      2863px 744px #fff, 497px 1229px #fff, 298px 264px #fff, 2311px 600px #fff,
      1557px 1253px #fff, 87px 673px #fff, 401px 1682px #fff, 2450px 1438px #fff,
      1902px 1515px #fff, 1631px 1883px #fff, 687px 671px #fff,
      2478px 1712px #fff, 2470px 920px #fff, 632px 1818px #fff, 89px 1594px #fff,
      2043px 1140px #fff, 63px 688px #fff, 1726px 922px #fff, 2076px 1909px #fff,
      2234px 419px #fff, 476px 1730px #fff, 1053px 1137px #fff,
      1448px 1853px #fff, 173px 759px #fff, 2986px 40px #fff, 2778px 1837px #fff,
      2874px 1221px #fff, 572px 1338px #fff, 2014px 672px #fff, 984px 923px #fff,
      2277px 1656px #fff, 1602px 662px #fff, 1624px 1980px #fff,
      2378px 621px #fff, 1077px 1086px #fff, 724px 1275px #fff, 873px 529px #fff,
      821px 2000px #fff, 2917px 882px #fff, 1468px 919px #fff, 825px 1290px #fff,
      1810px 1693px #fff, 193px 353px #fff, 1262px 631px #fff, 2572px 154px #fff,
      1012px 431px #fff, 1787px 895px #fff, 129px 1395px #fff,
      2194px 1513px #fff, 415px 1678px #fff, 2698px 761px #fff;
  }
`;

export const Gnome = styled.div`
  ${({ theme }) => `
  position: absolute;
  bottom: 200px;
  left: ${theme.palette.type === "dark" ? "50%" : "60%"};
  transform: ${
    theme.palette.type === "dark" ? "translateX(-50%)" : "translateX(0%)"
  };
  width: 600px;
  height: 500px;
  canvas{
    width: 100%;
    height: 100%;
  }
  @media (max-height: 950px) {
    bottom: 100px;
    left: ${theme.palette.type === "dark" ? "50%" : "50%"};
  }
  @media (max-width: 1400px) {
    bottom: 150px;
  }
  @media (max-width: 1024px) {
    width: 100%;
    height: 400px;
    bottom: 30px;
    left: 0px;
    transform: none;
  }
  @media (max-width: 600px) {
    width: 100%;
    height: 250px;
    bottom: 20px;
    left: 0px;
    transform: none;
  }
  `}
`;

export const ButtonHolder = styled.div`
  position: absolute;
  bottom: 50px;
  z-index: 150;
  @media (max-width: 600px) {
    bottom: 5px;
  }
`;

const shootingTime = "3000ms";

export const nthChildDelay = ({ count = 20 }) => {
  const styles = {};
  [...Array(count).keys()].forEach((_, index) => {
    if (index !== 0) {
      styles[`&:nth-child(${index})`] = {
        animationDelay: `${Math.random() * 9999}ms`,
        top: `calc(50% - ${Math.random() * 600 - 200}px)`,
        left: `calc(40% - ${Math.random() * 1500}px)`,
        "&::before,&::after": {
          animationDelay: `${Math.random() * 9999}ms`,
        },
      };
    }
  });
  return styles;
};

export const ShootingStar = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;

  .night {
    position: relative;
    width: 200%;
    height: 100%;
    transform: rotateZ(45deg);
    left: -50%;
  }

  .shooting_star {
    position: absolute;
    left: 50%;
    top: 50%;
    height: 2px;
    background: linear-gradient(-45deg, rgba(255,255,255, 1), rgba(0, 0, 255, 0));
    border-radius: 999px;
    filter: drop-shadow(0 0 6px rgba(105, 155, 255, 1));
    animation:
            tail ${shootingTime} ease-in-out infinite,
            shooting ${shootingTime} ease-in-out infinite;

    &::before {
      content: '';
      position: absolute;
      top: calc(50% - 1px);
      right: 0;
      // width: 30px;
      height: 2px;
      background: linear-gradient(-45deg, rgba(0, 0, 255, 0), rgba(255,255,255, 1), rgba(0, 0, 255, 0));
      transform: translateX(50%) rotateZ(45deg);
      border-radius: 100%;
      animation: shining ${shootingTime} ease-in-out infinite;
    }

    &::after {
      content: '';
      position: absolute;
      top: calc(50% - 1px);
      right: 0;
      // width: 30px;
      height: 2px;
      background: linear-gradient(-45deg, rgba(0, 0, 255, 0), rgba(255,255,255, 1), rgba(0, 0, 255, 0));
      // transform: translateX(50%) rotateZ(45deg);
      border-radius: 100%;
      animation: shining ${shootingTime} ease-in-out infinite;
      transform: translateX(50%) rotateZ(-45deg);
    }

    ${nthChildDelay({ count: 10 })};

    @keyframes tail {
      0% {
        width: 0;
      }

      30% {
        width: 100px;
      }

      100% {
        width: 0;
      }
    }

    @keyframes shining {
      0% {
        width: 0;
      }

      50% {
        width: 30px;
      }

      100% {
        width: 0;
      }
    }

    @keyframes shooting {
      0% {
        transform: translateX(0);
      }

      100% {
        transform: translateX(300px);
      }
    }

    @keyframes sky {
      0% {
        transform: rotate(45deg);
      }

      100% {
        transform: rotate(45 + 360deg);
      }
    }
`;

/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { Box, Container, Grid, Typography } from "@material-ui/core";
import { useRef } from "react";
import { useRive } from "rive-react";
import RiveAnimation from "./images/gnome2.riv";
import {
  Background,
  Gnome,
  Head,
  Shadow,
  ShootingStar,
  Stars,
  Stone,
} from "./Header.style";

function Header() {
  const fStar = useRef(null);
  const paramsLight = {
    src: RiveAnimation,
    autoplay: true,
    animations: "light",
  };

  const paramsDark = {
    src: RiveAnimation,
    autoplay: true,
    animations: "dark",
  };

  const { RiveComponent: RiveLight } = useRive(paramsLight);
  const { RiveComponent: RiveDark } = useRive(paramsDark);

  return (
    <Head>
      <ShootingStar>
        <div className="night">
          <div className="shooting_star" />
          <div className="shooting_star" />
          <div className="shooting_star" />
          <div className="shooting_star" />
          <div className="shooting_star" />
          <div className="shooting_star" />
          <div className="shooting_star" />
          <div className="shooting_star" />
          <div className="shooting_star" />
          <div className="shooting_star" />
          <div className="shooting_star" />
          <div className="shooting_star" />
          <div className="shooting_star" />
          <div className="shooting_star" />
          <div className="shooting_star" />
        </div>
      </ShootingStar>
      <Stars ref={fStar}>
        <div id="stars" />
        <div id="stars2" />
        <div id="stars3" />
      </Stars>
      <Background data-id="background">
        <Gnome data-id="gnome">
          <RiveDark
            css={(theme) => css`
              position: absolute;
              width: 100%;
              height: 100%;
              z-index: ${theme.palette.type === "dark" ? 10 : -1};
            `}
          />

          <RiveLight
            css={(theme) => css`
              position: absolute;
              width: 100%;
              height: 100%;
              z-index: ${theme.palette.type === "dark" ? -1 : 10};
            `}
          />
        </Gnome>
        <Shadow data-id="shadow" />
        <Stone data-id="stone" />
      </Background>
      <Container
        css={css`
          position: relative;
          z-index: 100;
        `}
        maxWidth="lg"
      >
        <Grid container spacing={2} justify="center">
          <Typography variant="h1" component="h1">
            <Box px={[2, 2, 0]} fontSize={[80, 146, 146]}>
              <Box component="span" color="#ffffff">
                My Mini{" "}
              </Box>
              <span
                css={(theme) => css`
                  color: ${theme.palette.type === "dark" ? "#fee17f" : "#000"};
                `}
              >
                Miners
              </span>
            </Box>
          </Typography>
          <Typography variant="subtitle1" component="h2" gutterBottom>
            <Box px={[2, 2, 0]}>
              A decentralized NFT collector’s game on the{" "}
              <b>Polygon(Matic) chain</b>
            </Box>
          </Typography>
          {/* <ButtonHolder>
            <Button
              css={css`
                background-color: #20c8d5;
                border-radius: 6px;
              `}
              variant="contained"
              color="primary"
            >
              <img src={pancakeswap} width={32} alt="pancakeswap" />
              <Box px={1}>PURCHASE</Box>
            </Button>
          </ButtonHolder> */}
        </Grid>
      </Container>
    </Head>
  );
}

export default Header;

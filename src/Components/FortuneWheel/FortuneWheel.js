/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { Box, Container, Grid, Typography } from "@material-ui/core";
import { useTheme } from "@material-ui/core/styles";

function FortuneWheel() {
  const { palette } = useTheme();
  return (
    <div
      css={(theme) => css`
        min-height: 720px;
        background-color: ${theme.palette.type === "dark" ? "#000" : "#f7f7f7"};
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        background-image: url(${theme.palette.type === "dark"
          ? require("./images/spin-bg.png").default
          : require("./images/spin-light-bg.png").default});
      `}
    >
      <Container maxWidth={false} disableGutters>
        <Grid container justify="center" alignItems="center">
          <Box clone order={[2, 1, 1]}>
            <Grid container item xs={12} md={6} justify="flex-end">
              <Box p={[5, 6, 8]}>
                <Typography variant="h3" component="h3" gutterBottom>
                  <span
                    css={(theme) => css`
                      color: ${theme.palette.type === "dark" ? "#fff" : "#000"};
                    `}
                  >
                    Wheel of <br />
                  </span>
                  <span
                    css={() => css`
                      color: #fff;
                    `}
                  >
                    Gnomes
                  </span>
                </Typography>
                <Typography variant="subtitle1" component="h4" gutterBottom>
                  The place to get to the most adorable gnomes ever!!
                </Typography>
                <Typography variant="body1" component="p" gutterBottom>
                  <Box maxWidth={555}>
                    Take your chance on the wheel of gnomes, find and collect
                    new ones, and hope to get the most out of your spin.
                    <br />
                    Every spin wins!!! Yes, you hear it right; no matter where
                    the needle drops, you win a gnome chest.
                    <br />
                    Would you be the ultimate gnome collector? <br />
                  </Box>
                </Typography>
              </Box>
            </Grid>
          </Box>
          <Box clone order={[1, 2, 2]}>
            <Grid
              item
              container
              xs={12}
              md={6}
              css={() => css`
                img {
                  width: 100%;
                }
              `}
            >
              <img
                src={
                  palette.type === "dark"
                    ? require("./images/spin-light.png").default
                    : require("./images/spin-light.png").default
                }
                alt="community"
              />
            </Grid>
          </Box>
        </Grid>
      </Container>
    </div>
  );
}

export default FortuneWheel;

import { Box, Container, Grid, Typography } from "@material-ui/core";
import SocialButtons from "../UI/SocialButtons/SocialButtons";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";
import { useTheme } from "@material-ui/core/styles";

function Community() {
  const { palette } = useTheme();
  return (
    <div
      css={(theme) => css`
        background-color: ${theme.palette.type === "dark" ? "#000" : "#f7f7f7"};
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-image: url(${theme.palette.type === "dark"
          ? require("./images/dark-bg.png").default
          : "none"});
      `}
    >
      <Container maxWidth={false} disableGutters>
        <Grid container justify="center" alignItems="center">
          <Grid
            container
            item
            xs={12}
            md={6}
            alignItems="center"
            css={(theme) => css`
              background-color: ${theme.palette.type === "dark"
                ? "transparent"
                : "#fff"};
              img {
                width: 100%;
              }
            `}
          >
            <img
              src={
                palette.type === "dark"
                  ? require("./images/dark-phone.png").default
                  : require("./images/community-light.png").default
              }
              alt="community"
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Box p={8}>
              <SocialButtons />
              <Typography variant="h3" component="h3" gutterBottom>
                <span
                  css={(theme) => css`
                    color: ${theme.palette.type === "dark"
                      ? "#fee17f"
                      : "#df665f"};
                  `}
                ></span>
                <Box component="span">Comm</Box>
                <span
                  css={(theme) => css`
                    color: ${theme.palette.type === "dark"
                      ? "#fee17f"
                      : "#df665f"};
                  `}
                >
                  unity
                </span>
              </Typography>
              <Typography variant="body1" component="p" gutterBottom>
                <Box maxWidth={555}>
                  We believe that a Great game is a mere reflection of its
                  community. That`s why we will do our best to build the best
                  community the crypto world has ever seen.
                  <br />
                  Join us on social media to get the latest updates. <br />
                  <br />
                  Take part in helping the game get better by suggesting new
                  features and vote for existing suggestions.
                  <br />
                  Share it with other crypto fans around the world and make My
                  Mini Miners the best crypto game in the world!!!
                </Box>
              </Typography>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export default Community;

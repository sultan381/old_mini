import { Box, Container, Grid, Typography } from "@material-ui/core";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";

function About() {
  return (
    <div
      css={(theme) => css`
        display: flex;
        align-items: center;
        justify-content: center;
        height: 450px;
        background-color: ${theme.palette.type === "dark" ? "#000" : "#fff"};
        background-size: ${theme.palette.type === "dark" ? "cover" : "contain"};
        background-repeat: no-repeat;
        background-position: center;
        background-attachment: ${theme.palette.type === "dark"
          ? "fixed"
          : "initial"};
        background-image: url(${theme.palette.type === "dark"
          ? require("./images/about-dark.png").default
          : require("./images/about-light.png").default});
      `}
    >
      <Container maxWidth="lg">
        <Grid container spacing={2} justify="center" alignItems="center">
          <Box p={[3, 0, 0]} pt={3} align="center">
            <Typography variant="h3" component="h3" gutterBottom>
              <Box component="span">It’s All About </Box>
              <span
                css={(theme) => css`
                  color: ${theme.palette.type === "dark"
                    ? "#fee17f"
                    : "#df665f"};
                `}
              >
                Fun & Crypto
              </span>
            </Typography>
            <Typography variant="body1" component="p" gutterBottom>
              <Box fontWeight={400}>
                <b>
                  My Mini Miners brings a new way for players to collect
                  adorable gnomes on the blockchain.
                </b>
                <br />
                As a player, you will be able to create new gnomes, merge them
                into more powerful gnome and trade them in-game or other on
                platforms. You will also earn Matic every day for playing.{" "}
                {/* <Box component="span" color="#df665f"> */}
                {/*  we plan to add more and more mini-games <br /> that will */}
                {/*  increase the gaming experience to the next level. */}
                {/* </Box> */}
              </Box>
            </Typography>
          </Box>
        </Grid>
      </Container>
    </div>
  );
}

export default About;

import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTwitter, faTelegramPlane } from "@fortawesome/free-brands-svg-icons";
import styled from "@emotion/styled";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";
/** @jsxImportSource @emotion/react */
import { css } from "@emotion/react";

export const Icons = styled.div`
  font-size: 29px;
`;

export const Icon = styled(FontAwesomeIcon)`
  color: ${(props) => props.color};
  font-size: 29px;
`;

const useStyles = makeStyles({
  root: {
    padding: "8px",
  },
});

function SocialButtons({ color = "#b2b2b2" }) {
  const classes = useStyles();
  return (
    <Icons>
      {/* <IconButton
        classes={{
          root: classes.root,
        }}
        href="#"
        rel="nofollow"
        target="_blank"
       >
        <Icon color={color} icon={faDiscord} />
       </IconButton> */}
      <IconButton
        classes={{
          root: classes.root,
        }}
        rel="nofollow"
        target="_blank"
        href="https://twitter.com/myminiminers"
      >
        <Icon
          color={color}
          css={css`
            :hover {
              color: #1da1f2;
            }
          `}
          icon={faTwitter}
        />
      </IconButton>
      <IconButton
        classes={{
          root: classes.root,
        }}
        rel="nofollow"
        target="_blank"
        href="https://t.me/myminiminers"
      >
        <Icon
          color={color}
          css={css`
            :hover {
              color: #0088cc;
            }
          `}
          icon={faTelegramPlane}
        />
      </IconButton>
      {/* <IconButton
        classes={{
          root: classes.root,
        }}
        rel="nofollow"
        target="_blank"
        href="https://reddit.com/submit?url=https://www.cryptognomes.com&title=Mine%20crypto%20while%20you%20sleep"
       >
        <Icon color={color} icon={faReddit} />
       </IconButton> */}
    </Icons>
  );
}

SocialButtons.propTypes = {
  color: PropTypes.string,
};

export default SocialButtons;

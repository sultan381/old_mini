import CssBaseline from "@material-ui/core/CssBaseline";
import { useEffect, useState } from "react";
import { ThemeProvider as MuiThemeProvider } from "@material-ui/styles";
import { ThemeProvider } from "@emotion/react";
import { createMuiTheme } from "@material-ui/core/styles";
import { dark, light } from "./theme";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Home } from "./Pages";
import { TopBar } from "./Components";
import WebFont from "webfontloader";
import ReactGA from "react-ga";
const TRACKING_ID = "UA-200623099-2"; // YOUR_OWN_TRACKING_ID
ReactGA.initialize(TRACKING_ID);

function App() {
  const [theme, setTheme] = useState(true);

  const appliedTheme = createMuiTheme(theme ? light : dark);

  useEffect(() => {
    WebFont.load({
      google: {
        families: ["Antonio", "Open Sans"],
      },
    });
  }, []);
  useEffect(() => {
    // This line will trigger on a route change
    ReactGA.pageview(window.location.pathname + window.location.search);
  });

  return (
    <MuiThemeProvider theme={appliedTheme}>
      <ThemeProvider theme={appliedTheme}>
        <CssBaseline />
        <TopBar theme={theme} setTheme={setTheme} />
        <Router>
          <Switch>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Router>
      </ThemeProvider>
    </MuiThemeProvider>
  );
}

export default App;

const typography = {
  fontFamily: "Antonio, Arial",
  h1: { fontSize: "9.125rem", fontWeight: 700 },
  h3: { fontSize: "3.625rem", fontWeight: 400 },
  subtitle1: {
    fontFamily: "Open Sans",
    fontSize: "1.75rem",
    fontWeight: 400,
  },
  body1: {
    fontFamily: "Open Sans, Arial",
    fontSize: "1.125rem",
    fontWeight: 400,
  },
  body2: {
    fontFamily: "Open Sans, Arial",
  },
  caption: {
    fontFamily: "Open Sans, Arial",
  },
  overline: {
    fontFamily: "Open Sans, Arial",
  },
};

export const light = {
  typography,
  palette: {
    type: "light",
    background: {
      default: "#fff",
    },
  },
};

export const dark = {
  typography,
  palette: {
    type: "dark",
    background: {
      default: "#230d39",
    },
  },
};

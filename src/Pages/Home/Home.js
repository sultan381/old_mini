import {
  About,
  FortuneWheel,
  Header,
  PlayerAndInvestors,
  Community,
  Footer,
} from "../../Components";

function Home() {
  return (
    <>
      <Header />
      <About />
      <PlayerAndInvestors />
      <FortuneWheel />
      <Community />
      <Footer />
    </>
  );
}

export default Home;
